import random

MENU_ITEM = {
    1 : "Start game",
    2 : "View statistics",
    3 : "Exit"
}

FILE_STATISTICS = "One-armed bandit\\statistics.txt"

def write_to_file(line):
    with open(FILE_STATISTICS, "a") as file:
        file.write(line + "\n")

def start_game():
    print("Game started")
    values = [random.randint(1, 10) for _ in range(3)]
    print("Values:", values)
    write_to_file(f"Values: {values}")
    if all(value == values[0] for value in values):
        print("\033[92mYou WIN.\033[0m")
        write_to_file("All values are the same. You WIN.")
    else:
        print("\033[91mYou LOSE.\033[0m")
        write_to_file("Values are not all the same. You LOSE.")
    

def view_statistics():
    print("\033[93mStatistics:\033[0m")
    with open(FILE_STATISTICS) as file:
        print(file.read())

    print("\033[93mEnd of statistics\033[0m")
    
#Start program

while True:
    for key, value in MENU_ITEM.items():
        print(f"{key}. {value}")

    result = input("Enter the number of the desired menu item: ")
    result = int(result)

    if result == 1:
       start_game()

    elif result == 2:
        view_statistics()

    elif result == 3:
        print("Exit")
        exit()