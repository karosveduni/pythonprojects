import os

FILE_NAME = 'C:\\Users\\USER_NAME\\source\\repos\\Python Projects\\ClearFIleTemp\\FilePath.txt'

MENU_ITEMS = {}

result = 0

def remove_files_and_folder():
    line = MENU_ITEMS[result][0]
    remove_files_and_folder_with_parameter(line)

def remove_files_and_folder_with_parameter(line):
    file_list = os.listdir(line)
    for file in file_list:
        file_path = os.path.join(line, file)
        os.remove(file_path)

def clear_all_folders():
    lines = list(MENU_ITEMS.values())
    print('\033[91mClearing all folders...\033[0m')
    for line in lines:
        remove_files_and_folder(line[0])
    print('Clearing all folders completed!')

#Start program

with open(FILE_NAME) as file:
    for index, line in enumerate(file, start = 1):
        MENU_ITEMS.update({index : (line.strip(), remove_files_and_folder)})

if not MENU_ITEMS:
    print('\033[91mNo folders to clean\033[0m')
    exit()
else:
    size = len(MENU_ITEMS)
    MENU_ITEMS.update({size + 1 : ('Clear all folders', clear_all_folders)})
    MENU_ITEMS.update({size + 2 : ('Exit', exit)})

while True:
    for key, value in MENU_ITEMS.items():
        print(f'{key}. {value[0]}')

    result = input('Select the folder to be cleaned: ')
    result = int(result)

    MENU_ITEMS[result][1]()


