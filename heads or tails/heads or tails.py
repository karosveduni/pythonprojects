import random
import datetime

MENU_ITEM = {
    1 : "Heads or Tails",
    2 : "Exit"
}

results = []

FILE_RESULTS = 'heads or tails\\results game.txt'

def heads_or_tails():
    result = random.choice(["Heads", "Tails"])
    return result

def write_to_file(line):
    with open(FILE_RESULTS, 'a') as file:
        file.write(line + '\n')

#Start program
    
while True:

    for key, value in MENU_ITEM.items():
        print(f"{key}. {value}")

    choice = input("Enter the number of the desired menu item: ")

    if choice == "1":
        print("Heads or Tails")
        outcome = heads_or_tails()
        results.append(outcome)
        current_time = datetime.datetime.now().strftime("%H:%M:%S")
        write_to_file(f'Outcome: {outcome}, Time: {current_time}')
        print(outcome)
        if len(results) == 7: 
            if all(item == results[0] for item in results):
                print('You get the same side 7 times in a row and you WIN the jackpot.')
                write_to_file(f'You WIN!!! Res: {results}, Time: {current_time}')

    elif choice == "2":
        print("Exit")
        print("Results:", results)
        exit()